export interface TodoModel {
  name: string;
  completed: boolean;
  isDeleted?: boolean;
  createdAt?: string;
  updatedAt?: string;
}
