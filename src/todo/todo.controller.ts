import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { TodoService } from './todo.service';
import { TodoDto } from './todo.dto';

@Controller({
  version: '1',
  path: 'todo',
})
@ApiTags('Todo ')
export class TodoController {
  constructor(private todoService: TodoService) {}
  @Get('/list')
  @ApiOperation({ summary: 'list all todo' })
  listPagination() {
    return this.todoService.list();
  }

  @Post('')
  @ApiOperation({ summary: 'create todo' })
  async createTodo(@Body() todo: TodoDto): Promise<any> {
    return await this.todoService.createTodo(todo);
  }

  @Put('/:id')
  async triggerCompleteTodo(@Param('id') id: string) {
    return await this.todoService.triggerCompleteTodo(id);
  }

  @Delete('/:id')
  async deleteTodo(@Param('id') id: string) {
    return await this.todoService.deleteTodo(id);
  }
}
