import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { TodoModel } from 'src/models/todo.model';

export type TodoDocument = HydratedDocument<Todo>;

@Schema({ timestamps: true, _id: true })
export class Todo implements TodoModel {
  @Prop()
  name: string;

  @Prop({ default: false })
  completed: boolean;

  @Prop({ default: false })
  isDeleted?: boolean;
}

export const TodoSchema = SchemaFactory.createForClass(Todo);
