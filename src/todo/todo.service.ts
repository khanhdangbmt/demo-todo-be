import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { Todo } from './todo.schema';

@Injectable()
export class TodoService {
  constructor(@InjectModel(Todo.name) private todoModel: Model<Todo>) {}

  async getTodo(todoId: string) {
    return await this.todoModel.findById(todoId);
  }

  async createTodo(todo: any) {
    const result = (await this.todoModel.create(todo)).toJSON();
    return {
      status: HttpStatus.OK,
      data: result,
    };
  }

  async deleteTodo(todoId: any) {
    const todo = await this.todoModel
      .findById(new mongoose.Types.ObjectId(todoId))
      .lean();

    const result = await this.todoModel.updateOne(
      { _id: todo._id },
      {
        isDeleted: true,
      },
    );
    return {
      status: result.modifiedCount ? HttpStatus.OK : HttpStatus.NOT_FOUND,
    };
  }

  async list() {
    const listTodos = await this.todoModel.find({ isDeleted: false }).lean();

    return {
      data: listTodos,
      status: HttpStatus.OK,
    };
  }

  async triggerCompleteTodo(todoId: string) {
    console.log('🚀 ~ TodoService ~ todoId:', todoId);

    const todo = await this.todoModel
      .findById(new mongoose.Types.ObjectId(todoId))
      .lean();

    console.log('🚀 ~ TodoService ~ todo:', todo);

    const result = await this.todoModel.updateOne(
      { _id: todo._id },
      {
        completed: !todo.completed,
      },
    );
    console.log('🚀 ~ TodoService ~ result:', result);

    return {
      status: result.modifiedCount ? HttpStatus.OK : HttpStatus.NOT_FOUND,
    };
  }
}
